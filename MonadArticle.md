Monads: A useful tool for the working programmer?
=================================================

TLDR
----

Monads are a not a very useful abstraction for programmers:

* Monads can be a confusing concept to get to grips with 
* The 'Monad Laws' seem arbitrary and irrelevant to the goals of monads
* It isn't clear what the most important examples of monads actually have in common

Haskell adopted monads as an abstraction because it faced a fundamental conflict
between its ideal computational world of pure, lazy function composition and
the messy reality of a time- and order- dependent, uncertain, stateful real world. 
In particular, how do you provide a way to interact with a user without
destroying most of the benefits of functional programming.

The solution Haskell adopted came originally from a highly abstract field of 
mathematics and that legacy means that the terminology is often confusing for
programmers.
But the actual mechanisms that monads provide have proved to be a rather simple
and elegant way to deal with three common types of complexity:

1. Handling interactive or time-dependent events (eg. user interaction)
2. Managing complex conditional decision paths (eg. mode-dependent behaviours)
3. Adding additional capabilities to existing calculations (eg. logging)

These three forms of complexity aren't limited to Haskell - they are significant 
causes of complexity in programming in all languages. 
Mainstream programmers can benefit from understanding how monads reduce these
sources of complexity and take some of the lessons across to their own practice.

The most valuable concept is that of performing two separate channels of
computation in parallel.

By separating the administration of conditional logic, state, interactions and time-dependencies,
the programmer can focus on expressing the business logic of the program 
as a simple, linear sequence of functions.

Meanwhile the monad is taking care of the complicated parts: performing the interactions,
and sequentially stitching together all of the administrative information it needs from
step to step. Meanwhile, it provides the functions in the main channel of computation
with the input values they need, when they need them, as if they were normal data 
that had come through the normal chain of composed functions.

While you may not want to reimplement monads in your preferred language, this
idea can be used to great advantage in reducing complexity in any program.

YAMA: yet another monad article
-------------------------------

The world isn't exactly crying out for yet another article on monads. 
One of the creators of Haskell (Philip Wadler) wrote a perfectly
good piece on the subject 
(which you can find [here](http://homepages.inf.ed.ac.uk/wadler/papers/marktoberdorf/baastad.pdf "Monads for functional programming")).
So one might expect that would already be sufficient. But the
constant flow of blog posts streaming into Hacker News and clogging up your news
feed shows that no one has quite nailed the task of explaining monads to the
general working programmer.

Some of the more serious attempts to explain monads begin by telling you the 'Monad Laws'
and continue on to provide the four canonical examples of monads as if we can 
somehow grok the concept by seeing the inherent similarity between them. 

This reminds me of a story told by Richard Feynman about reviewing a school 
physics textbook. The text book presented pictures of a robot dog, a real dog
and a wind up toy and asked "What makes them move?". The answer, apparently, is
'energy'! Similarly, I'm pretty sure no-one is going to be helped in identifying
the Platonic ideal abstraction shared by the IO, Option and State monads by seeing
them next to each other.

Other articles are 'Category Theory 101's that discuss such things as 
'Applicative Functors' and similar jargonesque nonsense. 
I'm a mathematician and I admit to being completely dulled out by this stuff. 
Besides, how will it help me write better programs?

Most common are the 'bad metaphor' articles. It seems
as if every programmer who experiments with Haskell has to write a blog post
giving their personal 'take' on what monads 'really' are as they try to verbalise
their embryonic efforts to come to terms with monads. Typically they end up with
some variation on the 'monads are like wrappers' metaphor. 

This reminds me of the slew of 'objects are boxes of data and code' articled
back in the day when Object Oriented Programming was a new thing. Once again,
the writers grasping for an understanding confused the mechanism for the
insight. When OOP pioneer, Alan Kay talks about Objects, he isn't talking about
namespaces and data structures, he talks about communities of independent 'cells'
that send messages to each other. 
(A good example is [here](http://userpage.fu-berlin.de/~ram/pub/pub_jf47ht81Ht/doc_kay_oop_en "The Meaning of Object Oriented Programming"))

Just as the 'code + data' aspect of OOP misses the core concept, 
the 'wrapper-like' aspect of monads is probably its least interesting and least
useful feature.

Hopefully, this discussion gives you a different perspective that will allow
you to take something of practical value back to your daily programming practice.

Personally, I think there is something very profound and exciting in the 
discoveries made by Haskell programmers that can make all of our programs
a little less complicated. That's got to be a good thing.

Why is Haskell pure and lazy?
-----------------------------

**Pure** languages offer a number of advantages. Knowing that a calculation
will always produce the same result means that values can be cached,
and that the calculation can be performed at any time, including before run time,
or delayed until its value is actuqally required.
Another benefit for modern computers is that calculation can often be performed
in parallel, since there is no risk of one calculation altering the result of
another.
Further, it is much easier to verify that a program is 'correct' (ie. it does
what it was designed to do) since one can look at each component separately
without having to worry about complicated interactions changing the outcome.
As a result, pure functional code is typically much easier to understand
and reason about, and tends to contain far fewer subtle bugs.
An added benefit, these features allow compilers to make far more aggressive
optimisations that is usually possible in 'shared state' languages like C.

**Lazy** languages do not perform calculations immediately, but defer them
until a value is actually needed. 
Sometimes this can mean that a function never has to run at all. 
Or calculations that may never end (such as generating infinite lists) can still
produce partial results without having to worry about getting stuck in a 
never-ending loop.
Moreover, the program isn’t constrained to waiting for external data to proceed: 
functions that don't need the missing data can still be calculated and 
functions can still be composed together ready to act 'all at once' when the
data is available.

Haskell is therefore radically different from most other programming languages,
and certainly very different from the 'mainstream' industrial languages such as
Java and C++, which operate as sequential machines performing one operation after
another.

Why is that a problem?
----------------------

The issue for Haskell is that purity and laziness severely restrict what a Haskell
program can do.

If the program is used only to transform data that already exists at the start
of execution, then Haskell can produce elegant, efficient, easy-to-reason about
programs. 

The problem is that the majority of software does not proceed in this
way. 
While Haskell programs are in some sense 'time independent',
the outside world where most of the data live is clearly not.
In general, we *don't* know all of the inputs before we run a program and
we expect a program to interact with its users, with hardware, with databases,
with networks which all operate in real time.

For Haskell, dealing with the outside world means confronting two problems:

1. Time- and order- dependent events

    This is most obvious when a human interacts with a program. The user probably
    hasn't decided what input to provide until after they see the output from 
    an intermediate stage of the calculation. A computer game in Haskell would need
    to ensure the sequence: 'show enemy', 'aim', 'fire', 'kill enemy' is strictly
    preserved and coordinated with 'real-time' physics. Since we cannot know in 
    advance what decision the user will make when presented with a choice,
    we cannot compose our calculations in the way we would like.

2. Uncertainty

    IO operations might fail, networks have long lag times, and devices have unknown state.
    Some programs (eg. Monte Carlo simulations) are even deliberately non-deterministic.
    Others may follow radically different paths depending on the exact
    nature of features in the data that may not be fully known until
    part-way through the calculation. This means that we cannot fully
    compose calculations without taking into account all the possible ways
    that the data can be arranged and all possible situations that may arise. 
    Even worse, this is often a combinatorial problem since multiple uncertainties
    may compound the number of separate paths the calculation may end up following.
    It would be neither practical nor pleasant to write code that mixed up
    the calculation with tracking every possibility explicitly.

Why Monad's were introduced into Haskell
----------------------------------------

The IO Monad was introduced into Haskell to solve some of these problems: trying to
allow interaction with the outside world without destroying all the benefits of
pure, lazy functional programming that Haskell provides. It would be extremely
limiting if Haskell were only useful for mathematics and data processing tasks.

### How monads fix things

'Monads' were introduced as an idea taken from a paper by Eugenio Moggi, who
in turn drew on ideas in an area of abstract mathematics called Category Theory.
Monads add a mechanism that can deal with composition of the messy, sequential
and interactive parts of a program 
without altering the nice, composable functional parts. 

Effectively, monads provide a 'side channel' for dealing with the sequential
elements while maintaining the ability to compose functions together in a 
linear pipeline.
Despite the fact that monads carry additional information and perform other tasks,
they retain the one-input, one-output (hence the 'mono-' in mon-ads) nature of
function composition.

One way to view it ias that the monad alters some of the pure functions in the 
pipeline so they can deal with the sequential or stateful steps. 
This is analagous to the decorators in Python which modify a function without
altering its code. Or similar to 'aspect-oriented' systems, like AspectJ, that
add orthogonal capabilities to existing classes and functions.

To understand this better, we can look at what happens when a function needs
to interact with a user. 
The Monad machinery sends some output to the console and holds up the 
calculation of its next function until the user provides a value. It
then makes this value available to the function exactly as if it had been just
another value passing down the chain from one function to the next and had been
known about since the beginning of the program.

Another way tothink about running a Haskell program with this interaction
is as having two distinct phases. 
In the first, all the components are composed together and as many
calculations performed as possible. Effectively it creates a single
'super function' representing the program. 
Then in the second phase, when you provide a value, it will send it to this function
and complete the dependent calculations to produce your result.

### What actually got added to Haskell

When we say that "monads" were added to Haskell, we actually mean that
three specific capabilities were added:

1.  The `monad` typeclass

    The monad type is a mechanism for taking existing data types that can be
    produced and consumed with pure, lazy functions, and altering them
    so that related, modified or additional information can be carried
    along through the calculations. 
    
    A key element of this is that when given a function that takes a value
    of the original type, the monad is able to provide it with an appropriate value.

2.  The bind operator `>>=`

    The bind operator stitches together calculations so that the modified
    or additional information produced by one function can be combined with
    information carried over from the previous step or from outside without
    altering the functions themselves.
    
    Specifically, it provides a `join` function that takes the input monad and
    the output from the function to produce a new monad. In this way it can
    carry over information from previous steps and perform additional
    calculations through a 'second channel' specific to the kind of monad, not
    to the functions.
    
    This way we can separate the code that deals with the administration (in the
    `join` function) from the functional logic of the program itself that operates
    on simple values. Unlike the pure, lazy functions in the primary channel, 
    the join is stateful, sequential and eager.

3.  `do` syntax for sequential ordering of binds

    Finally, a new syntax feature was added to the Haskell language to simplify
    writing a sequence of binds. In particular it allows intermediate steps to
    be named and used later, which would otherwise require a long set of nested
    lambda functions to achieve.
    
    The result is that programmers can write the sequential parts of
    the program in much the same way as in a typical imperative programming
    language.

Other Monads
------------

Once monads were added to Haskell for IO, it became obvious that the
concept was far more general. Indeed any situation in which state or
uncertainty were barriers to pure functional composition could be 
expressed more simply using the same mechanism.

### The Error, Option and Writer monads

One of the most difficult programming problems is to deal with a series
of operations which may fail. For example, in file IO operations we
cannot be sure the disk will operate, that it can be written to, whether
a file exists, whether the file is correctly formatted or correctly
located, whether it has appropriate permissions or is in use.
Therefore a typical situation in which we write data to a file can fail
in a large number of ways and most of these will require some clean up.

This clean-up and error-checking code is often messy: it is order-dependent,
full of nested tests and conditional resource-cleanup. Since many of the code
paths are rarely used, it often hides bugs which because they involve resources
can be subtle and hard to detect.

Some of the solutions for this problem in mainstream languages include:
Python's context managers,
Java's checked exceptions with 'finally:' blocks,
the C idiom of returning a status while returning data via pointers,
and a 'permissable' use of `goto` in C.

Monads solve this problem in a different way. We can write the program as
if the steps all proceeded correctly and deal with the possibility of problems,
and the required bookkeeping, with the monad's 'second channel'. 

The Option monad allows a calculation
to fail and all future calculations will simply be bypassed.

The Writer monad accumulates error message strings, which can be accessed
once all the calculations are complete.

A full Error monad would also keep track of what recovery and resource clean-up
is required.

The critical idea is that rather than dealing with a complex maze of possible
control paths, the programmer only needs to worry about a single, linear path,
uncluttered by dealing with the possibility of failure. And since the program
is just another functional chain, Haskell can perform all of its usual magic.

This is so much neater than common alternatives, that mainstream languages are
moving in the direction of Haskell, at least in this use case. 
Option types of some description are now available in most of the mainstream 
OO languages and all of the big new ones.

### The State monad

A common pattern in programming is that the choice of which action to
take depends on a 'state machine'. Typically we have some state
representing which 'mode' or 'phase' we are currently in and the
calculation we make depends on this. Indeed one of the very first programs
ever written, Turing's Universal Computer, is essentially written as a state machine.

A more familiar example is the vim editor. The meaning of
a key stroke depends critically on what mode we happen to be in at the time
(rendering vim so hard to use for novices that Apple included a specific
recommendation promoting 'modeless' programming in its influential Human Interface
Guidelines). In vim, if we are in NORMAL mode, `d` initiates the delete command
and waits for a parameter; in VISUAL mode, `d` deletes the selected block of
text, and in INSERT mode, `d` inserts the character `d` at the cursor point.

In any case, programming state machine behaviour is often tedious, ugly and
error prone - we need to include separate tests in each possible mode and
provide transitions between modes as well as the modal functionality at each
point.

Instead, Haskell offers the opportunity to have the functionality separated from
the bookkeeping of modal transitions by composing 'state transformers' using
monads. The program can take a starting state and the composed state transformers
track the state at each stage and have the correct code run at each step
once the initial state is known.

### The List monad

Another common pattern in calculations that requires significant bookkeeping
is the management a set of multiple possible outcomes in a calculation. 

For example, a Sudoku solver needs to maintain a list of possible values
for each cell at each stage of the calculation. It must allow the possibility
that any of these values could be correct until they are eliminated.

Languages such as Prolog have built-in mechanisms for dealing with this
situation. For other languages, the typical approach is to work with lists of values.
At each step every possibility it attempted in turn and a back-tracking stack 
is used to return when a contradiction is reached. Results from all the successful
results then need to be combined back together into a new list.

The List monad offers a way to separate the calculation steps from the 
administration of the possible values. The monad can manage the backtracking, 
iteration and list flattening, while the programmer focuses on the 
calculations as if only one value needed to be checked.

A more general perspective on monads
------------------------------------

Given how useful monads have proven and the range of situations they have been
useful in, it is natural for Haskell adepts would want to
generalise the concept as far as possible. 

The monad typeclass is available as a general mechanism that can be used 
by programmers in any situation that conforms to the three "Monad Laws".
Essentially this stipulate that:
* a monad can be constructed from a value of the original type
* that it can provide a value of the original type to a function that requires it
* it provides a sensible way to join the results together from step to step

### Monads are not a useful abstraction for programmers

The fact that the various monads listed enjoy the happy coincidence of all
being expressible using the same mechanism doesn't demonstrate that there is some
intrinsic property of these situations that is in some way 'monadic'. 
But Haskell adepts have a tendency to try to expound it as if it were a profound truth. 

This may be so: in maths coincidences often reflect some deeper
truth in this way. But for the working programmer looking for 
practical and useful tools, it is confusing, misleading and off-putting.

### The specific use cases

Rather than attempting to find some unifying principle, I find it easier to
look at the distinct patterns where monads provide some useful functionality.

1.  Pattern 1: Uncertain external inputs

    When inputs are external, uncertain or time-dependent,
    monads allow the programmer to write imperative-looking sequential
    code to deal with this interaction, while preserving the functional
    composition as much as possible in the rest of the program and to proceed 
    as as if all of the inputs were really known in advance. 
    
    Human interaction is the most obvious use case, but network and device
    IO also follow this pattern too.

2.  Pattern 2: Linearising multiple decision paths

    Probably the greatest cause of program complexity stems from
    having to manage conditional code that depends on details of the
    input data, on interactions, or on state, since this can create a maze of
    possible execution paths through the code, making it hard to understand, 
    hard to debug, and hard to test.
    
    The most common non-self-inflicted causes are dealing with modal behavior, 
    device state, and possible errors.
    
    Most of these situations can be 'linearised' by separating the 'main flow'
    functionality from the administration of tracking the conditional state.
    
    The Option and State monads are the most immediately helpful examples.

3.  Pattern 3: Separating bookkeeping tasks from calculation

    We often wish to add a single capability to several different functions and 
    objects. Examples include logging, networking, security etc. 
    Typically we are forced to insert additional code into each object 
    or function separately and intermingle this new 'concern' with the
    'business logic' of the object.
    
    The Writer monad is an example of how monads can provide logging capabilities
    while leaving the underlying calculation flow unaltered.
    
    Given that this is the simplest use of monads it is often presented as a
    motivating example. But strictly speaking, we do not need monads for this
    at all as it is a fully deterministic capability and and not necessarily sequential.
    It could just as easily be done using a purely functional wrapper.
    
    However since monads already provide a neat syntax for performing 
    administrative tasks in a secondary channel, monads can be coopted 
    as a convenient mechanism for carrying the additional data through the
    calculations and performing bookkeeping operations on it.

Are monads of any relevence to the rest of us?
-----------------------------------------------------

As we have seem, Haskell needs monads to solve a particular conflict within the
language. But is there any benefit to the rest of us for whom
uncertain, sequential decisions are the default assumption of the
languages they use?

The general concept of trying to write programs as much as possible as a
linear composition of pure functional, single-concern elements is great advice
for any programmer in any language. Such programs are far easier to read, debug,
test and reason about. We should all strive to make our functions
purer, smaller, simpler, with fewer conditional paths. (The book 
[Clean Code](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882 "Clean Code at amazon.com"))
gives similar advice.)

Equally, programmers should do their best to separate the flow of the 
business logic from the complexity-inducing dealings with stateful processes, 
uncertain or conditional outcomes, external processes, time-dependent orderings
and orthogonal concerns.

'Monadic composition' is the idea of providing a linear, purely 
functional composition of operations, while managing the
underlying causes of complexity in a parallel 'channel'. 
It has nothing directly to do with Category Theory, or Monad Laws, except that
this happens to be the path by which this idea found its way into Haskell.

Specifically, the big innovation is the use of a `bind` operator which manages 
the two computation 'channels' in parallel: 

1.  The pure functional path  

    A value is lazily provided to a pure function and its result passed on to the
    next step. These steps can be composed easily, a range of optimisations
    performed, and programmers can follow the business logic easily.
    
2.  The bookkeeping path

    The more complex, uncertain, interdependent, or conditional elements are 
    managed by sequentially stitching the results from each step together using
    code that relates only to the type of complexity being managed, 
    not the business logic.

It is unlikely that monads will make it into mainstream languages any time soon,
and I don't propose for programmers to reimplement monads in all of their 
programs. Instead, programmers can use the concept of separating the business
logic from the management of complexity using a the neat concept of a `bind`
operator to greatly simplify their code in a many different situations. 

I have found the concept to be enormously useful in simplifying
interactions with failure-prone hardware in C, a language about as far removed
from the academic pedestal of Haskell type theory as one can imagine.

(To illustrate this, I have provided a simple toy example in my
[GitLab repository](https://gitlab.com/QuesterZen/MeaningOfMonads "Meaning Of Monads")
which illustrates using a separate calculation channel operating on a 
metadata object to manage IO failures and resource cleanup 
without needing to provide conditional code.
To compile it use: `gcc Example.c Metadata.c FileReader.c -o example`.
To run it: `./example [error]`.
where error simulates a failure; 0 is no error, 1 is open file failure, 
`s0i` creates an error in stage s on line read iteration i
)

It would be a shame if the idea failed to find greater adoption because of
confusion caused by the usual pseudo-mathematical nonesense
and erroneous metaphors typically associated wth the term 'monad'. 
