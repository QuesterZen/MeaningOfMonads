// Monad Example in C
// Exploration of a 'two channel' approach to complexity-reduction
// Author: James Leibert (questerzen@protonmail.com)
// Created: 3 August 2018

// Copyright James Leibert 2018
// Code available under GPL3 Licence

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "FileReader.h"

int
show_file(char* filename)
{
  RA_meta md = create_metadata(0, "", NULL, NULL);
  
  int fh;
  DO(fh, ra_open, filename, md)
  
  char* line;
  char* parsed;
  int record_dummy;
  do {
    DO(line, ra_readline, fh, md)
    DO(parsed, ra_parse, line, md)
    DO(record_dummy, ra_display_records, parsed, md)
  } while (md->status == 0 && !is_eof(line));
  
  char msg[2048];
  int status = return_metadata(md, msg, 2000);
  printf("%d - <%s>\n", status, msg);
  return status;
}

int
main(int argc, char* argv[])
{
  if (argc == 2) {
    error = (int) strtol(argv[1], (char **)NULL, 10);
  } else {
    error = 0;
  }
  return show_file("a filename");
}

