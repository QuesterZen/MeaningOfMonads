#include <stdio.h>
#include <string.h>
#include "Metadata.h"

static int file_handle = 1000;
int error = 0;

void
file_close(void * c)
{
	printf("CLEANUP: Closing file handle %d.\n", file_handle);
}

int
ra_open(char* filename, RA_meta md)
{
	int result = -1;
	
	int success = (error != 1);
	if (success) {
		file_handle = 9999;
		result = file_handle;
		join_metadata(md, RA_SUCCESS(&file_close, (void*)&file_handle));
	} else {
		join_metadata(md, RA_FAILURE(-1, "Failed to open file."));
	}
	
	return result;	
}

char *
ra_readline(int fileHandle, RA_meta md)
{
	char * result = NULL;
	
	static int counter = 0;
	static char buffer[200];
	counter ++;
	if (counter <= 10) {
		sprintf(buffer, "Line %d", counter);
	} else {
		buffer[0] = '\0';
	}
	int success = (error != 100 + counter);
	if (success) {
		result = buffer;
		join_metadata(md, RA_SUCCESS(NULL, NULL));
	} else {
		join_metadata(md, RA_FAILURE(-2, "Failed to read from file."));
	}
	
	return result;
}

int
is_eof(char* line)
{
	return (line == NULL || strlen(line) == 0);
}

char *
ra_parse(char* line, RA_meta md)
{
	char* result = NULL;

	static char buffer[200];
	if (!line || strlen(line) == 0) {
		strcpy(buffer, "EOF");
	} else {
		strncpy(buffer, line + 5, 200);	
	}

	int success = (error != 200 + (buffer[0] - '0'));
	if (success) {
		result = buffer;
		join_metadata(md, RA_SUCCESS(NULL, NULL));
	} else {
		join_metadata(md, RA_FAILURE(-3, "Failed to parse input string."));
	}
	return result;
}

int
ra_display_records(char* record, RA_meta md)
{
	int result = 0;
	
	if (record[0] == '\0') {
		printf("Display record -> EOF\n");
	} else {
		printf("Display record -> %s\n", record);	
	}

	int success = (error != 300 + record[0] - '0');
	if (success) {
		join_metadata(md, RA_SUCCESS(NULL, NULL));
	} else {
		join_metadata(md, RA_FAILURE(-4, "Failed to write record."));
	}
	return result;
}