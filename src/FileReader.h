// Header for File Reader functions
// Demonstrates monadic composition of error handling in a failure-prone IO process

// Author: James Leibert
// Created: 3 August 2018
// Copyright James Leibert 2018
// Code available under GPL3 licence

#include "Metadata.h"

// Simulate an error during file read
// ... 0 -> no failures
// ... 1 -> failure opening file
// ... 100 + i -> read failure on ith line
// ... 200 + i -> parse failure on ith line
// ... 300 + i -> display failure on ith line
extern int error;

// Open a file using its file name.
// Expects type 'string' as char*.
// Returns type 'fileHandle' as int.
int 
ra_open(char* filename, RA_meta md);

// Read one line from a file.
// Expects type 'fileHandle' as int.
// Returns type 'string' as char*.
char *
ra_readline(int fileHandle, RA_meta md);

// Checks for EOF as indicated by a zero-length readline.
// Expects type 'string' as char*.
// Returns 1 if EOF, 0 otherwise.
int
is_eof(char* line);

// Parse one line, looking for the line number.
// Expects type 'string' as char*.
// Returns type 'record' as char*.
char *
ra_parse(char* line, RA_meta md);

// Print the records from a line on the console.
// Expects type 'record' as char*.
// Returns type 'void' as NULL.
int
ra_display_records(char* record, RA_meta md);
