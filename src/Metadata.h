// Underlying mechanism for monads in C
// The metadata is a secondary, sequential, impure parallel channel through a calculation
// which manages its own administration, leaving the programmer free to compose the
// business logic in a pure, linear fashion.
// Implements:
// ... Option monad (status code) - functions are bypassed if status != 0
// ... Writer monad (messages) - accumulates error messages through functions
// ... Error monad (cleanups) - accumulates resource cleanup functions through functions in case of errors

// Author: James Leibert
// Created: 3 August 2018
// Copyright James Leibert 2018
// Code available under GPL3 licence

#define MAX_MESSAGE_SIZE 1024
#define MAX_CLEANUP_FNS 32

typedef void (* CleanUpFns)(void*);

// Metadata object to hold additional information
// for the Resource Allocator monad
struct RA_Metadata
{
    int metadata_tag;   
    int status;
    char messages[MAX_MESSAGE_SIZE];
    int message_length;
    CleanUpFns cleanup_fns[MAX_CLEANUP_FNS];
    void * cleanup_params[MAX_CLEANUP_FNS];
    int cleanup_functions;
};

typedef struct RA_Metadata* RA_meta;

// Create a unique new metadata object
RA_meta
create_metadata(int status,
                const char* initial_message,
                void (*cleanup_fn)(void *),
                void* cleanup_parameter);

// Monadic join function for metadata
// Returns the initial meta data object updated with information from
// the result and frees the result metadata object.
RA_meta
join_metadata(RA_meta initial,
              RA_meta result);

// Extract information from the metadata. Returns status and copies
// messages into the message_buffer. Calls all cleanup functions.
// Frees the meta data object.
int
return_metadata(RA_meta md, char* message_buffer, int buffer_length);


int
message_copy(char* destination,
             int destination_size,
             const char* source,
             int source_bytes,
             char terminator);
            
#define DO(name, funct, param, md) \
        if (md && md->status == 0) { \
            name = funct(param, md); \
        } else { \
        }

            
// Useful for generating a successful value in a function
// use NULL as the cleanup function if no action is required
#define RA_SUCCESS(cleanup_fn, cleanup_param)           \
        create_metadata(0, "", cleanup_fn, cleanup_param)

// Useful for generating a failure in a function
#define RA_FAILURE(status, message)                       \
        create_metadata(status, message, NULL, NULL)
