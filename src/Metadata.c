#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Metadata.h"

int
message_copy(char* destination,
             int destination_size,
             const char* source,
             int source_bytes,
             char terminator)
{
    int bytes = source_bytes < destination_size ? source_bytes : destination_size-1;
    strncpy(destination, source, bytes);
    destination[bytes] = terminator;
    return bytes;
}


RA_meta
create_metadata(int status,
                const char* initial_message,
                void (*cleanup_fn)(void *),
                void* cleanup_parameter)
{
    static int counter = 0;
    RA_meta md = (struct RA_Metadata*)calloc(1, sizeof(struct RA_Metadata));
    md->metadata_tag = counter++;
    md->status = status;
	md->message_length = 0;
	md->cleanup_functions = 0;
	md->messages[0] = '\0';
    if (initial_message != NULL && strlen(initial_message) > 0) {
        int written = message_copy(md->messages,
                                   MAX_MESSAGE_SIZE,
                                   initial_message,
                                   strlen(initial_message),
                                   '\0');
        md->message_length = written;        
    }
    if (cleanup_fn != NULL && md->cleanup_functions < MAX_CLEANUP_FNS) {
        md->cleanup_fns[md->cleanup_functions] = cleanup_fn;
        md->cleanup_params[md->cleanup_functions] = cleanup_parameter;
        md->cleanup_functions++;
    }
    return md;
}


RA_meta
join_metadata(RA_meta initial,
              RA_meta result)
{
    initial->status = result->status;
    int written = message_copy(initial->messages + initial->message_length,
                               MAX_MESSAGE_SIZE - initial->message_length,
                               result->messages,
                               result->message_length,
                               '\0');
    initial->message_length += written;
    
    for (int i = initial->cleanup_functions, j = 0;
         j < result->cleanup_functions;
         i++, j++) {
        initial->cleanup_fns[i] = result->cleanup_fns[j];
        initial->cleanup_params[i] = result->cleanup_params[j];
		initial->cleanup_functions++;
    }
    
    free(result);
    return initial;    
}

int
return_metadata(RA_meta md, char* message_buffer, int buffer_length)
{
    int result = md->status;
    
    message_copy(message_buffer,
				buffer_length,
				md->messages,
                md->message_length,
                '\0');

    for (int i = md->cleanup_functions - 1; i >= 0; i--) {
        md->cleanup_fns[i](md->cleanup_params[i]);
    }
    free(md);
    return result;
}

